from rest_framework import serializers

from src.posts.models import RefPostLikeModel


class LikeAnalyticSerializer(serializers.ModelSerializer):

    class Meta:
        model = RefPostLikeModel
        fields = ['day', 'cnt']
