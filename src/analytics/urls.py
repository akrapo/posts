from django.urls import path

from src.analytics.views import AnalyticsView

app_name = 'analytics'

urlpatterns = [
    path('', AnalyticsView.as_view()),
]
