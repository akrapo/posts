from dateutil import parser
from django.db.models import Count
from rest_framework.response import Response
from rest_framework.views import APIView

from src.analytics.serializers import LikeAnalyticSerializer
from src.posts.models import RefPostLikeModel


class AnalyticsView(APIView):
    serializer_class = LikeAnalyticSerializer

    def get(self, request):
        query = RefPostLikeModel.objects \
            .extra(select={'day': 'date( created_at )'}) \
            .values('day') \
            .annotate(cnt=Count('id'))

        date_from_str = request.query_params.get('date_from')
        if date_from_str:
            date_from = parser.parse(date_from_str)
            query = query.filter(created_at__gte=date_from)

        date_to_str = request.query_params.get('date_to')
        if date_to_str:
            date_to = parser.parse(date_to_str)
            query = query.filter(created_at__lte=date_to)

        result = query.all()
        return Response(result)
