from django.urls import path

from . import views
from .views import LikeListView, DoUnLikeView, DoLikeView, LikeFilteredListView

app_name = 'posts'

urlpatterns = [
    path('', views.PostView.as_view()),
    path('likes/', LikeListView.as_view()),  # GET: all
    path('<int:post_id>/likes/', LikeFilteredListView.as_view()),  # GET: filtered by post
    path('like/', DoLikeView.as_view()),  # POST: do like
    path('<int:post_id>/unlike/', DoUnLikeView.as_view()),  # DELETE (by query args): do unlike
]
