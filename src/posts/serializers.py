from rest_framework import serializers

from src.posts.models import PostModel, RefPostLikeModel
from src.users.models import CustomUser


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['id', 'username', 'email']


class PostSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = PostModel
        fields = ['id', 'title', 'content', 'user']


class RefPostLikeSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = RefPostLikeModel
        fields = '__all__'


class RefPostLikeDetailsSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = RefPostLikeModel
        fields = '__all__'
