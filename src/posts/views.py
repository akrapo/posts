from django.http import Http404
from rest_framework import generics, permissions

from src.posts.models import PostModel, RefPostLikeModel
from src.posts.serializers import PostSerializer, RefPostLikeSerializer, RefPostLikeDetailsSerializer


class PostView(generics.ListCreateAPIView):
    queryset = PostModel.objects.all().order_by('-created_at')
    serializer_class = PostSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class RefPostLikeView(generics.ListCreateAPIView):
    queryset = RefPostLikeModel.objects.all().order_by('-created_at')
    serializer_class = RefPostLikeSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class LikeListView(generics.ListAPIView):
    queryset = RefPostLikeModel.objects.all().order_by('-created_at')
    serializer_class = RefPostLikeDetailsSerializer


class LikeFilteredListView(generics.ListAPIView):
    serializer_class = RefPostLikeDetailsSerializer

    def get_queryset(self):
        post_id = self.kwargs.get('post_id')
        return RefPostLikeModel.objects.filter(post_id=post_id)


class DoLikeView(generics.CreateAPIView):
    serializer_class = RefPostLikeDetailsSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class DoUnLikeView(generics.DestroyAPIView, generics.RetrieveAPIView):
    serializer_class = RefPostLikeDetailsSerializer
    lookup_field = 'post_id'

    def get_object(self):
        post_id = self.kwargs.get('post_id')
        instance = RefPostLikeModel.objects\
            .filter(user__id=self.request.user.id)\
            .filter(post_id=post_id)\
            .order_by('-created_at').first()
        if not instance:
            raise Http404
        return instance
