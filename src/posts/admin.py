from django.contrib import admin

from src.posts.models import PostModel

admin.site.register(PostModel)
