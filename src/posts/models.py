from django.conf import settings
from django.db import models


class PostModel(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='posts', on_delete=models.CASCADE, null=True)

    title = models.CharField(max_length=50)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'posts'


class RefPostLikeModel(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='posts2', on_delete=models.CASCADE)
    post = models.ForeignKey(PostModel, related_name='likes2', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'ref_posts_likes'

    def __str__(self):
        return f'{self.id}, {self.created_at}'

