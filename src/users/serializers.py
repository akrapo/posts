from django.contrib.auth import password_validation
from django.utils.translation import ugettext as _
from rest_framework import serializers

from src.users.models import CustomUser


class UserSerializer(serializers.ModelSerializer):
    is_active = serializers.ReadOnlyField()
    is_staff = serializers.ReadOnlyField()
    last_login = serializers.ReadOnlyField()
    last_request = serializers.ReadOnlyField()

    password = serializers.CharField(
        label=_("Password"),
        help_text=password_validation.password_validators_help_text_html(),
        write_only=True,
    )

    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'password', 'is_active', 'is_staff', 'last_login', 'last_request')
        ref_name = 'CustomUserSerializer'

    def create(self, validated_data):
        user = CustomUser.objects.create_user(**validated_data)
        return user

    def validate(self, attrs):
        password_validation.validate_password(attrs['password'], self.instance)
        return attrs
