import datetime

from django.utils.deprecation import MiddlewareMixin

from src.users.models import CustomUser


class UpdateLastRequestMiddleware(MiddlewareMixin):
    def process_response(self, request, response):
        assert hasattr(request, 'user'), \
            'The UpdateLastActivityMiddleware requires authentication middleware to be installed.'
        if request.user.is_authenticated:
            CustomUser.objects.filter(id=request.user.id) \
                .update(last_request=datetime.datetime.utcnow())
        return response
