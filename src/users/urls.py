from django.contrib.auth.models import update_last_login
from django.urls import path, include
from rest_framework import routers
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_jwt.views import ObtainJSONWebToken

from src.users import views

app_name = 'users'

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register('', views.UserViewSet)


# django-rest-framework-jwt issue #235
# last_login does not get updated #235
# https://github.com/jpadilla/django-rest-framework-jwt/issues/235
class JWTSerializer(JSONWebTokenSerializer):
    def validate(self, attrs):
        validated_data = super().validate(attrs)
        update_last_login(None, validated_data['user'])
        return validated_data


class AccountLoginAPIView(ObtainJSONWebToken):
    serializer_class = JWTSerializer


urlpatterns = [
    path('profile/', include(router.urls)),
    path('login/', AccountLoginAPIView.as_view()),
]
