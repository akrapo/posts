from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _


class CustomUser(AbstractUser):
    last_request = models.DateTimeField(_('last request'), blank=True, null=True)
