## Post management

### Technologies:
* [Django Rest Framework](https://www.django-rest-framework.org/)
* [REST framework JWT Auth](https://jpadilla.github.io/django-rest-framework-jwt/)
* [drf-yasg - Yet another Swagger generator](https://drf-yasg.readthedocs.io/en/stable/readme.html)

### Installation:
    git clone <repo>
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt
    python manage.py migrate

### Run Application
    python cli/make_config_file.py
    python manage.py runserver
    python cli/demo_bot.py


### Basic Endpoints
* `GET: /swagger/` - show all endpoints in one page
* `GET: /api/posts/<post_id>/likes/` - get all likes for <post_id>
* `POST: /api/posts/like/` - like post
* `DELETE: /api/posts/<post_id>/unlike/` - unlike post
* `GET: /api/analytics/?date_from=<date>&date_to=<date>` - analytics of likes, grouped by day
* `GET: /api/users/profile/<user_id>/` - user's last activity: login and request



# TODO:
* apply permissions for uses's auth urls
* swagger page: describe and allow to send all query params
* make unittests
