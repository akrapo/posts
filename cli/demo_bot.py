import configparser
import time

import requests

config = configparser.ConfigParser()
config.read('config.ini')
config_properties = config['PROPERTIES']

NUMBER_OF_USERS = config_properties.getint('number_of_users')
MAX_POSTS_PER_USER = config_properties.getint('max_posts_per_user')
MAX_LIKES_PER_USER = config_properties.getint('max_likes_per_user')
USER_PASSWORD = 'testtest1234'

SERVER_URL = 'http://127.0.0.1:8000/'

SIGNUP_URL = f'{SERVER_URL}api/users/profile/'
LOGIN_URL = f'{SERVER_URL}api/users/login/'


def handle_response(result):
    # If the status code is between 200 and 400
    if not result.ok:
        raise Exception(result, result.text)


def get_microtime():
    return int(time.time() * 10 ** 6)


def signup():
    microtime = get_microtime()
    random_username = f'test{microtime}'
    user_data = {'username': random_username, 'password': USER_PASSWORD}
    response = requests.post(SIGNUP_URL, data=user_data)
    handle_response(response)
    return response.json()


def login(username):
    user_data = {'username': username, 'password': USER_PASSWORD}
    response = requests.post(LOGIN_URL, data=user_data)
    handle_response(response)
    return response.json()


def make_post(token):
    payload = {'title': 'test title', 'content': 'test post 1234'}
    headers = {'Authorization': f'JWT {token}'}
    response = requests.post(f'{SERVER_URL}api/posts/', data=payload, headers=headers)
    handle_response(response)
    return response.json()


def like_post(post_id, token):
    payload = {'post': post_id}
    headers = {'Authorization': f'JWT {token}'}
    response = requests.post(f'{SERVER_URL}api/posts/like/', data=payload, headers=headers)
    handle_response(response)
    return response.json()


if __name__ == "__main__":
    # 1. signup
    usernames = []
    for _ in range(NUMBER_OF_USERS):
        result_data = signup()
        usernames.append(result_data['username'])
    print(f'There was created: {len(usernames)} users')

    # 2. Login
    users_tokens = []
    for username in usernames:
        result_data = login(username)
        users_tokens.append(result_data['token'])
    print(f'There was logged in: {len(users_tokens)} users')

    # 3. Make a new post
    posts_ids = []
    for token in users_tokens:
        for _ in range(MAX_POSTS_PER_USER):
            result_data = make_post(token)
            posts_ids.append(result_data['id'])
    print(f'There was published: {len(posts_ids)} posts')

    likes_ids = []
    for token in users_tokens:
        user_likes_cnt = 0
        for post_id in posts_ids:
            user_likes_cnt += 1
            if user_likes_cnt > MAX_LIKES_PER_USER:
                continue
            result_data = like_post(post_id, token)
            likes_ids.append(result_data['id'])
    print(f'There was liked: {len(likes_ids)} posts')
