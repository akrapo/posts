import configparser

config = configparser.ConfigParser()

PROPERTIES = {
    'number_of_users': 10,
    'max_posts_per_user': 10,
    'max_likes_per_user': 20,
}

config['PROPERTIES'] = PROPERTIES

with open('config.ini', 'w') as configfile:
    config.write(configfile)
