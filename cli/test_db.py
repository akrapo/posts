from django.db.models import Count
from django.db.models.functions import TruncDay

from src.posts.models import RefPostLikeModel

posts = RefPostLikeModel.objects\
    .extra(select={'day': 'date( created_at )'})\
    .values('post_id', 'day')\
    .annotate(cnt=Count('id'))\
    .filter(created_at__gte='2020-05-21')\
    .filter(created_at__lte='2020-05-26')\
    .all()
print('posts', posts.query)
# print('posts', posts)

for p in posts:
    print('p', p)
